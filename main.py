__variable = private
variable = public


class Student:
    def __init__(self, name: str = "John Doe", course: str = "Basic Science", gpa: float = 0.00) -> none:
        self.__name = name
        self.__course  = course
        self.__validate_gpa_is_float(gpa)
        self.__gpa = gpa

    def __validate_gpa_is_float(self, gpa):
        #check if gpa is numeric, if not numeric raise an error
        if not isinstance(gpa, (int, float)):
            raise TypeError(f"{gpa} should be float")

    def __validate_gpa_is_within_limits(self, gpa):
        if gpa < 1 or gpa > 5:
            raise ValueError(f"{gpa} is not valid gpa")


    def class_of_student_grade(self):
        self.__validate_gpa_is_within_limits(self.__gpa)
        #4-5 = excellent, 3-4 = good, 2-3 = avarage, poor
        if self.__gpa >= 4.5
            return "Excellent"
        elif self.__gpa >= 3.5 and self.__gpa < 4.5:
            return "Good"
        elif self.__gpa >= 2.5 and self.__gpa <= 3.5:
            return "Average"
        else:
            return "Try harder"

    def get_gpa_rank(self):
        return self.class_of_student_grade(self.__gpa)


cal = Student(name= "Kent", course= "CIS", gpa= 4.5)
cal2 = Student(name= "Lambikukk", course= "CS", gpa= 3.5)